﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    public class UI
    {
        public Notebook n1 = new Notebook();
        public List<Note> notes = new List<Note>();
        static void Main(string[] args)
        {
            UI act = new UI();
            act.Action();
        }
        public void Action()
        {
            Console.WriteLine("\n" + "Создать запись (1)");
            Console.WriteLine("Отредактировать данные (2)");
            Console.WriteLine("Удалить запись (3)");
            Console.WriteLine("Просмотреть все записи (4)");
            Console.WriteLine("Просмотреть краткую информацию (5)");
            Console.Write("\n" + "ВВЕДИТЕ НОМЕР ДЕЙСТВИЯ: ");
            int input = Int32.Parse(Console.ReadLine());
            Console.WriteLine();

            switch (input)
            {
                case 1:
                    notes.Add(n1.CreateNote());
                    break;
                case 2:
                    n1.EditNote(notes);
                    break;
                case 3:
                    n1.DeleteNote(notes);
                    break;
                case 4:
                    n1.ShowAllNotes(notes);
                    break;
                case 5:
                    n1.ShowBrief(notes);
                    break;
            }

            Console.WriteLine("\n" + "Вернуться в меню? (да|нет) ");
            string st = Console.ReadLine();
            if (st == "да") Action();
        }
    }

    public class Note
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Phone { get; set; }
        public string Country { get; set; }
        public DateTime Birthday { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string Notes { get; set; }
        public Note(int id, string lastname, string firstname, string phone, string country, string middlename=default, DateTime birthday=default, string organization=default, string position=default, string notes=default)
        {
            this.ID = id;
            this.LastName = lastname;
            this.FirstName = firstname;
            this.Phone = phone;
            this.Country = country;
            this.MiddleName = middlename;
            this.Birthday = birthday;
            this.Organization = organization;
            this.Position = position;
            this.Notes = notes;
        }
    }
    public class Notebook
    {
        public List<Note> one = new List<Note>();
        public int id = 0;
        public DateTime birthday;
        public string lastname, firstname, middlename, phone, country, organization, position, notes, bd;
        public Note CreateNote ()
        {
            id++; 
            Console.WriteLine($"Введите Фамилию (#{id}):");
            lastname = Console.ReadLine();
            Console.WriteLine("Введите Имя:");
            firstname = Console.ReadLine();
            Console.WriteLine("Введите Отчество:");
            middlename = Console.ReadLine();
            bool isNum;
            do
            {
                Console.WriteLine("Введите телефон:");
                phone = Console.ReadLine();
                isNum = int.TryParse(phone, out int num);
            } while (!isNum);

            Console.WriteLine("Введите дату рождения (формат хх.хх.хххх):");
            bd = Console.ReadLine();
            if (DateTime.TryParse(bd, out _))
            {
                birthday = Convert.ToDateTime(bd);
            }
            Console.WriteLine("Введите страну:");
            country = Console.ReadLine();
            Console.WriteLine("Введите организацию:");
            organization = Console.ReadLine();
            Console.WriteLine("Введите должность:");
            position = Console.ReadLine();
            Console.WriteLine("Введите дополнительную информацию:");
            notes = Console.ReadLine();
            Note note = new Note(id, lastname, firstname, phone, country, middlename, birthday, organization, position, notes);
            return note;
        }
        public void EditNote(List<Note> allnotes)
        {
            string s;
            List<Note> all = new List<Note>(allnotes);
            do
            {
                Console.Write("\n" + "Введите номер редактируемого: ");
                int number = Convert.ToInt32(Console.ReadLine());
                foreach (var item in all)
                {
                    if (item.ID == number)
                    {
                        id = number;
                        Note n = new Note(id, lastname, firstname, phone, country, middlename, birthday, organization, position, notes);
                        n.ID = item.ID;
                        allnotes.Insert(number, Update(n, ref number));
                        allnotes.Remove(item);
                    }
                }
                Console.WriteLine("\n" + "Отредактировать ещё? (да|нет) ");
                s = Console.ReadLine();
            } while (s == "да" && allnotes.Count > 0);
        }
        public Note Update (Note nt, ref int number)
        {
            nt.ID = number;
            Console.WriteLine($"Введите Фамилию:");
            nt.LastName = Console.ReadLine();
            Console.WriteLine("Введите Имя:");
            nt.FirstName = Console.ReadLine();
            Console.WriteLine("Введите Отчество:");
            nt.MiddleName = Console.ReadLine();
            bool isNum;
            do
            {
                Console.WriteLine("Введите телефон:");
                phone = Console.ReadLine();
                isNum = int.TryParse(phone, out int num);
            } while (!isNum);
            nt.Phone = phone;

            Console.WriteLine("Введите дату рождения (формат хх.хх.хххх):");
            bd = Console.ReadLine();
            if (DateTime.TryParse(bd, out _))
            {
                birthday = Convert.ToDateTime(bd);
            }
            nt.Birthday = birthday;

            Console.WriteLine("Введите страну:");
            nt.Country = Console.ReadLine();
            Console.WriteLine("Введите организацию:");
            nt.Organization = Console.ReadLine();
            Console.WriteLine("Введите должность:");
            nt.Position = Console.ReadLine();
            Console.WriteLine("Введите дополнительную информацию:");
            nt.Notes = Console.ReadLine();
            return nt;
        }
        public void DeleteNote(List<Note> allnotes)
        {
            string s;
            List<Note> all = new List<Note>(allnotes);
            do
            {
                Console.Write("\n" + "Введите номер удаляемого: ");
                int number = Convert.ToInt32(Console.ReadLine());
                foreach (var item in all)
                {
                    if (item.ID == number)
                        allnotes.Remove(item);
                }
                Console.WriteLine("\n" + "Удалить ещё? (да|нет) ");
                s = Console.ReadLine();
            } while (s == "да" && allnotes.Count > 0);
        }
        
        public void ShowAllNotes(List<Note> allnotes)
        {
            var sortedUsers = allnotes.OrderBy(u => u.ID);
            foreach (Note u in sortedUsers)
            {
                Console.WriteLine(u.ID + "). " + u.LastName + " - " + u.FirstName + " - " + u.MiddleName + " - " + u.Phone + " - " + u.Birthday.ToShortDateString() + " - " + u.Country + " - " + u.Organization + " - " + u.Position + " - " + u.Notes);
            }
        }
        public void ShowBrief(List<Note> allnotes)
        {
            var sortedUsers = allnotes.OrderBy(u => u.ID);
            foreach (Note u in sortedUsers)
            {
                Console.WriteLine(u.ID + "). " + u.LastName + " - " + u.FirstName + " - " + u.Phone);
            }
        }
    }


}
